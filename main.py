#!/usr/bin/python
#  -*- coding: utf8 -*-
from flask import Flask, request, render_template, jsonify
from flask_cors import CORS, cross_origin
from ParseJson import JsonParser
from configuration import config as cnf


app = Flask(__name__)


@app.route('/', methods=['POST'])
def receive_json():
    json_content = request.json
    # print(request.json["status"])
    #Display json content
    #print("INcoming json ->> ",json_content)
    #Process Json .1.Firing alerts - 2. Resolved alerts
    parsejsonobject = JsonParser()
    parsejsonobject.parser(json_content)

    return "OK"

#########################################################


##########################################################

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=cnf.PORT, threaded=True)


