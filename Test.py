import requests

from models.MetricInfoModel import MetricInfo

str = """
"condition": {
                "category": "logical_operation",
                "id": 65,
                "inequality": null,
                "logical_operation": {
                    "condition_list": [
                        {
                            "condition": {
                                "category": "inequality",
                                "id": 66,
                                "inequality": {
                                    "LHS": {
                                        "expression": {
                                            "alert": null,
                                            "category": "metric",
                                            "constant": null,
                                            "function": {
                                                "group_by_label_key_list": [
                                                    "EDIT"
                                                ],
                                                "id": 46,
                                                "name": "avg"
                                            },
                                            "id": 81,
                                            "mathematical_operation": null,
                                            "metric": {
                                                "duration": {
                                                    "id": 68,
                                                    "unit": "m",
                                                    "value": 5
                                                },
                                                "id": 42,
                                                "label_list": [
                                                    {
                                                        "label": {
                                                            "id": 59,
                                                            "key": "instance",
                                                            "matching_operator": "=",
                                                            "value": "192.168.1.10"
                                                        }
                                                    }
                                                ],
                                                "name": "cpu_used_percentage",
                                                "offset": {
                                                    "id": 48,
                                                    "unit": "m",
                                                    "value": 0
                                                }
                                            },
                                            "promql": null
                                        }
                                    },
                                    "RHS": {
                                        "expression": {
                                            "alert": null,
                                            "category": "constant",
                                            "constant": 80,
                                            "function": null,
                                            "id": 82,
                                            "mathematical_operation": null,
                                            "metric": null,
                                            "promql": null
                                        }
                                    },
                                    "comparison_operator": ">",
                                    "id": 41
                                },
                                "logical_operation": null,
                                "promql": null
                            }
                        },
                        {
                            "condition": {
                                "category": "inequality",
                                "id": 67,
                                "inequality": {
                                    "LHS": {
                                        "expression": {
                                            "alert": {
                                                "duration": {
                                                    "id": 69,
                                                    "unit": "m",
                                                    "value": 5
                                                },
                                                "id": 3,
                                                "label_list": [
                                                    {
                                                        "label": {
                                                            "id": 60,
                                                            "key": "instance",
                                                            "matching_operator": "=",
                                                            "value": "192.168.1.10"
                                                        }
                                                    }
                                                ],
                                                "name": "DOS_Attack_Alert",
                                                "offset": {
                                                    "id": 49,
                                                    "unit": "s",
                                                    "value": 0
                                                }
                                            },
                                            "category": "alert",
                                            "constant": null,
                                            "function": null,
                                            "id": 83,
                                            "mathematical_operation": null,
                                            "metric": null,
                                            "promql": null
                                        }
                                    },
                                    "RHS": {
                                        "expression": {
                                            "alert": null,
                                            "category": "constant",
                                            "constant": 1,
                                            "function": null,
                                            "id": 84,
                                            "mathematical_operation": null,
                                            "metric": null,
                                            "promql": null
                                        }
                                    },
                                    "comparison_operator": "==",
                                    "id": 42
                                },
                                "logical_operation": null,
                                "promql": null
                            }
                        }
                    ],
                    "id": 29,
                    "logical_operator": "OR"
                },
                "promql": null
            }
"""



metric_list = []
metric_list = handle_condition(condition, metric_list)

# Now for whole list, send to prometheus promql

body = []
for metric in metric_list:
    req_metric_name = metric.metric_name
    req_metric_ip = metric.metric_ip
    req_duration_unit = metric.duration_unit
    req_duration_value = metric.duration_value
    # string_promql = "avg_over_time {0} {1}".format(a, b)
    string_promql = "avg_over_time(" + req_metric_name + "{instance%3D~" + "\"" + req_metric_ip + ".*\"}" + "[" + \
                    str(req_duration_value) + req_duration_unit + "])"
    print(string_promql)
    prom_respone = requests.get("http://localhost:9090/api/v1/query?query=" + string_promql)
    Fresponse = prom_respone.json()
    second_level = Fresponse['data']
    third_level = second_level['result']
    fourth_level = third_level[0]['value']
    final_value = fourth_level[1]
    # refill List
    metric.pure_value = final_value

#=======================================================================================
def metric_extractor(category):
    if category == "metric" :
        metric = self.exp['metric']
        name = metric['name']
        duration = metric['duration']
        firing_unit_alert = duration['unit']
        firing_value_alert = duration['value']
        metric_name = metric['name']
        label_list = metric['label_list']
        label = label_list[0]['label']
        value_ip = label['value']
        fir_alert = MetricInfo(name,value_ip,firing_unit_alert,firing_value_alert)
        return fir_alert

    return None

def handle_condition(condition, temp_list):

    if condition['category'] == "inequality":
        x = condition['inequality']
        lhs = x['LHS']
        rhs = x['RHS']
        self.expr = rhs['expression']
        catr = self.expr['category']
        self.exp = lhs['expression']
        cat = self.exp['category']
        if cat == "metric":
            firing_object1 = self.metric_extractor(cat)
            temp_list.append(firing_object1)
        if catr == "metric":
            firing_object2 = self.metric_extractor(catr)
            temp_list.append(firing_object2)

    elif condition['category'] == "logical_operation":
        for inner_condition in condition["logical_operation"]["condition_list"]:
            temp_list += self.handle_condition(inner_condition, temp_list)

    return temp_list