from datetime import datetime

utc_time = datetime.strptime("2017-11-02T19:15:37.808512423+02:00"[:19], "%Y-%m-%dT%H:%M:%S")
epoch_time = (utc_time - datetime(1970, 1, 1)).total_seconds()


print(int(round(epoch_time)))


{
  "name": "MA_DDOS_Attack",
  "description": "This is a DDOS attack",
  "alert_rule_id": 80,
  "confidence": 100,
  "validity_time": 60,
  "state": 1,
  "start_time": 1509712912,
  "alert_metadata_list": [
    {"metric_value": "1",
      "metric_name": "IDS_DDOS_Alert",
      "parameter_list": [
        {"label": {"value": "node", "key": "job"}},
        {"label": {"value": "vm", "key": "category"}},
        {"label": {"value": "10.100.80.52:10000", "key": "instance"}},
        {"label": {"value": "10.100.80.220", "key": "ip_src_0"}},
        {"label": {"value": "10.101.80.22", "key": "ip_src_1"}},
        {"label": {"value": "10.102.80.223", "key": "ip_src_2"}},
        {"label": {"value": "192.168.2.5", "key": "ip_src_3"}},
        {"label": {"value": "1000001", "key": "signature_id"}}
      ]
    },
    {"metric_value": "92",
      "metric_name": "cpu_used_percent",
      "parameter_list": [
        {"label": {"value": "node", "key": "job"}},
        {"label": {"value": "vm", "key": "category"}},
        {"label": {"value": "10.100.80.52:10000", "key": "instance"}}
      ]
    },
    {"metric_value": "83",
      "metric_name": "ram_used_percent",
      "parameter_list": [
        {"label": {"value": "node", "key": "job"}},
        {"label": {"value": "vm", "key": "category"}},
        {"label": {"value": "10.100.80.52:10000", "key": "instance"}}
      ]
    }
  ]
}