


class MetricInfoModel:

    def __init__(self,metric_name, metric_ip, duration_unit, duration_value,pure_value = None):
        self.metric_name = metric_name
        self.duration_unit = duration_unit
        self.duration_value = duration_value
        self.metric_ip = metric_ip
        self.pure_value = pure_value

    def __str__(self):
        return "metric_name" + self.metric_name + ", metric_ip" + self.metric_ip + ", duration_unit" + self.duration_unit + ", duration_value" + str(self.duration_value) + ", pure_value" + str(self.pure_value)