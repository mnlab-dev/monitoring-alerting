import requests

from models.MetricInfoModel import MetricInfoModel
from models.ResolvedAlertModel import ResolvedAlertModel
from datetime import datetime

class JsonParser:
    #'for every received json parse it and check 1.Firing and 2.Resolved'

    firing_alert_list = []
    firing_id_list = []
    expr = ""
    exp = ""



    def parser(self, jsonfile):

        # Turns '2017-11-02T17:25:52.803+02:00' format to epoch seconds: 1509643552 of type int
        def date_string_to_epoch_seconds(date_string):
            utc_time = datetime.strptime(date_string[:19], "%Y-%m-%dT%H:%M:%S")
            return round((utc_time - datetime(1970, 1, 1)).total_seconds())

        resid_list = []
        for alert in jsonfile['alerts']:
            if alert["status"] == "firing":
                alert_name = alert["labels"]["alertname"]
                start_time = date_string_to_epoch_seconds(alert["startsAt"])
                # send request to python 8082 process, to get info(id,conditions) for the alert
                json_alert_name_filter = {"alert_rule": {"name": alert_name}}
                ####
                # The request runs on 10.30.0.242,so the target is the same IP.
                # The port 8082 refers to Monitorig-api module that runs also in same IP.
                # More specific the </get_alert_rule> in the url shows that
                # the monitoring-api will receive a json format with the alert name
                received_json = requests\
                    .post("http://localhost:8082/get_alert_rule", json=json_alert_name_filter)\
                    .json()
                # print(r.status_code, r.reason)
                ####
                # The response is a json and it can be found of the <JsonExample_get_alert_rule.json> File
                ####
                # parse received_json and extract id and conditions
                # name_of_alert = received_json[0]['alert_rule']['name']
                if received_json:
                    metric_list = self.handle_condition(received_json[0]['alert_rule']['condition'])
                    # Now for whole list, send to prometheus promql

                    firing_alert = {
                        "alert_rule_id": received_json[0]['alert_rule']['id'],
                        "start_time": start_time,
                        "validity_time": 60,
                        "confidence": 100,
                        "alert_metadata_list": [],
                        "name": received_json[0]["alert_rule"]["name"],
                        "description": received_json[0]["alert_rule"]["annotation_description"]
                    }
                    body = []
                    print("to metric einai --------**************>>")
                    for metric in metric_list:
                        # A better approach -> string_promql = "avg_over_time {0} {1}".format(a, b)
                        string_promql = "avg_over_time(" + metric.metric_name + \
                                        "{instance%3D~" + "\"" + metric.metric_ip + ".*\"}" + "[" + \
                                        str(metric.duration_value) + metric.duration_unit + "])"
                        print(string_promql)
                        ####
                        # The request below is running to a vm 10.30.0.242. In same ip runs Prometheus.
                        # So we send this request to the Prometheus service at 9090 port.
                        # The variable <string_promql> is referring to the firing alert expressed to Promql language
                        ####
                        prometheus_response = requests\
                            .get("http://localhost:9090/api/v1/query?query=" + string_promql)\
                            .json()
                        ####
                        #The prometheus_response is a json of this format (For example) :
                        # {"status": "success", "data": {"resultType": "vector", "result": [
                        #   {"metric": {"category": "vm", "instance": "10.30.0.242:9100", "job": "node"},
                        #    "value": [1503917822.555, "87.682416182217"]}]}}
                        ####
                        temp_metric = prometheus_response['data']['result'][0]['metric']
                        parameter_list_dict = []
                        for key in temp_metric.keys():
                            parameter_list_dict.append({"label": { "key": key, "value":  temp_metric[key] }})
                        # refill List
                        metric.pure_value = prometheus_response['data']['result'][0]['value'][1]
                        firing_alert["alert_metadata_list"].append(
                            {
                            "metric_name": metric.metric_name,
                            "metric_value": metric.pure_value,
                            "parameter_list": parameter_list_dict
                            }
                        ) # me ta metrics
                        print("firing_alert final->>>>>")
                        print(firing_alert)
                        print("\n")
                        # the http request below is going to send to SPM
                        #The final format is the <firing_alert>
                    send_alert_2_local_db = requests.post("http://localhost:8082/create_alert_notification/v1",
                                                          json=firing_alert)
                    print("Sent to Local DB - Alert START")

                    send_alert_2_spm = requests.post("http://195.53.58.174:8080/spm/start_alert_notification",
                                                     json=firing_alert)
                    print("Sent to SPM - Alert START")

            elif alert["status"] == "resolved":
#                print(alert)
                end_time = date_string_to_epoch_seconds(alert["endsAt"])
                #print("onoma ---------------------------------------------------------------------****************-> ", alerts)
                received_json = requests.post("http://localhost:8082/get_alert_rule",
                                              json={"alert_rule": {"name": alert["labels"]["alertname"]}})\
                                        .json()
                if received_json:
                    id_resolved = received_json[0]['alert_rule']['id']
                    resid_list.append(ResolvedAlertModel(id_resolved, end_time))
                    data_json = {"alert_rule_id": id_resolved,
                                 "end_time": end_time,
                                 "validity_time": 60,
                                 "confidence": 100,
                                 "name": received_json[0]["alert_rule"]["name"],
                                 "description": received_json[0]["alert_rule"]["annotation_description"]
                                 }

                    send_alert_2_local_db = requests.put("http://localhost:8082/update_alert_notification/v1",
                                                          json=data_json)

                    print("Sent to Local DB - Alert END")


                    # the http request below is going to send to SPM
                    # The final format is the <data_json>
                    http_reply_for_resolved = requests.post("http://195.53.58.174:8080/spm/stop_alert_notification",
                                                            json=data_json)
                    print("Sent to SPM - Alert END")

        #end for loop of alerts


    #=======================================================================================

    def metric_extractor(self, metric):
        fir_alert = MetricInfoModel(metric['name'],
                               metric['label_list'][0]['label']['value'],
                               metric['duration']['unit'],
                               metric['duration']['value'])
        return fir_alert

    def handle_condition(self, condition):
        temp_list = []
        if condition['category'] == "inequality":
            #print("")
            #print("CASE: inequality")
            #print("")

            if condition['inequality']['LHS']['expression']['category'] == "metric":
                firing_object1 = self.metric_extractor(condition['inequality']['LHS']['expression']['metric'])
                temp_list.append(firing_object1)
            if condition['inequality']['RHS']['expression']['category'] == "metric":
                firing_object2 = self.metric_extractor(condition['inequality']['RHS']['expression']['metric'])
                temp_list.append(firing_object2)

        elif condition['category'] == "logical_operation":
            #print("")
            #print("CASE: logical_operation")
            #print("")
            for inner_condition in condition["logical_operation"]["condition_list"]:
                temp_list += self.handle_condition(inner_condition['condition'])
                #print("-==========================================")
                #for item in temp_list:
                    #print(item)
        return temp_list