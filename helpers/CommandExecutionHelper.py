import logging
import shlex
import subprocess
from configuration import config as cnf


MODE = "DEBUG"
# MODE = "INFO"


""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class CommandExecutionHelper:

    def __init__(self, state=None):
        self.state = state

    def command_execution(self, command_list):
        logger.info("BASH - Command: " + command_list[0])
        args = shlex.split(command_list[0])
        print(args)
        try:
            if args[0] == "sudo":
                logger.info("CASE SUDO ")
                (stdout, stderr) = subprocess.\
                    Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True).\
                    communicate(cnf.ROOT_PASSWORD + "\n")
            else:
                logger.info("CASE NOT SUDO ")
                (stdout, stderr) = subprocess.\
                    Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE).\
                    communicate()
            logger.info("STDOUT: " + stdout)
            logger.info("STDERR: " + stderr)
            # logger.info("STDERR: " + stderr)
            if command_list.__len__() > 1:
                (stdout, stderr) = self.command_execution(command_list[1:])
        except Exception as e:
            stdout = None
            stderr = e[-1]
        return stdout, stderr

    def command_execution_communicate(self, command_list, arg_to_communicate):
        logger.info("BASH - Command: " + command_list[0])
        args = shlex.split(command_list[0])
        print(args)
        try:
            logger.info("CASE NOT SUDO + ARG")
            (stdout, stderr) = subprocess.\
                Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True).\
                communicate(arg_to_communicate + "\n")
            logger.info("STDOUT: " + stdout)
            logger.info("STDERR: " + stderr)
            # logger.info("STDERR: " + stderr)
            if command_list.__len__() > 1:
                (stdout, stderr) = self.command_execution(command_list[1:])
        except Exception as e:
            stdout = None
            stderr = e[-1]
        return stdout, stderr