from contextlib import contextmanager

from sqlalchemy import create_engine, event
from sqlalchemy.orm import scoped_session, sessionmaker
#from configuration import config as cnf
#from models.Label import Label

Session = sessionmaker()

def on_connect(conn, record):
    conn.execute('pragma foreign_keys=ON')


#@contextmanager
#def db_session():
 #   """ Creates a context with an open SQLAlchemy session.
  #  """
   # mysqldbType = "mysql"
    #sqlitedbType = "sqlite"
    #sqlite_format = ":///"
    # Check if type of DB is sqlite or mysql
    #connection_string = None
    # empty string
    #engine = None

    #if cnf.DATABASE_TYPE == mysqldbType:
    #    connection_string = mysqldbType + cnf.DATABASE_CONN_STRING + "/" + cnf.DATABASE_NAME
    #    engine = create_engine(connection_string, echo=True)
    #elif cnf.DATABASE_TYPE == sqlitedbType:
    #    connection_string = sqlitedbType + sqlite_format + cnf.DATABASE_PATH
    #    engine = create_engine(connection_string, echo=True)
    #    event.listen(engine, 'connect', on_connect)
    #print(connection_string)


 #     connection = engine.connect()
 #     db_sess = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=engine))
 #     yield db_sess
 #     db_sess.remove()
 #     connection.close()


#def delete_label_orphans(session):
#   session.query(Label). \
#       filter(~Label.target_resource.any()). \
#       delete(synchronize_session=False)
#   session.query(Label). \
#       filter(~Label.expression_metric.any()). \
#       delete(synchronize_session=False)
#   session.query(Label). \
#       filter(~Label.expression_alert.any()). \
#       delete(synchronize_session=False)
#